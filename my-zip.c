#include <stdio.h>
#include <stdlib.h>

/*link used in order to find how to check if the next character is the same as the current one https://stackoverflow.com/questions/24282273/c-run-length-encoding-doesnt-work-on-large-file-worked-on-smaller-file*/


int main(int argc, char *argv[]){
	FILE *file;
	int i;
	int count,currentChar, nextChar;
	
	/*Checking if the file name is given*/
	if(argc<2){
		printf("my-zip: file1 [file2 ...]\n");
		exit(1);

	}
	else{
	
		for (i=1;i<argc;i++){
			file = fopen(argv[i],"r+");
		
			if(file == NULL){
				printf("my-zip: cannot open file.\n");
				exit(1);
			}
			
			currentChar = getc(file);
			nextChar = currentChar;
			/*Loop for checking if the next character is the same as the previous character until the file is ended*/
			while (nextChar != EOF){
			
				for (count = 0; nextChar == currentChar; count++){
					nextChar = getc(file);
				}
				
				fwrite(&count,sizeof(int),1,stdout);
				fputc(currentChar,stdout);
				currentChar = nextChar;
				
				
			
			}
				  fclose(file);
			
		
			
		}
		
	
	}
	exit(0);
	


}

