
/* author: Varpu Huhtinen 
Source used for the use of execv(): https://stackoverflow.com/questions/32142164/how-to-use-execv-system-call-in-linux
Source used for cd: https://www.daniweb.com/programming/software-development/threads/389378/chdir-in-c-program*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

//error handling messages
char parameter_message[40] = "A parameter error has occured.\n";
char error_message[30] = "An error has occured.\n";
char command_not_found[40]= "A command not found error has occured.\n";
char directory_message[40] = "A directory error has occured.\n";
char file_number_error[40] = "A file number error occured.\n";
char fork_error[30] = "Fork failed.\n";
char file_error[40] = "Cannot open the specified file.\n";

//function declaration
char * getPath(char *array[]);
void changeDir(char *array[]);
void executeCommand(char * path, char *array[]);

int main(int argc, char *argv[]){

	//setting the default path of the shell to /bin
	char * path ="/bin";
	
/*If there's a batch file specified, the program doesn't print a prompt*/
	if(argc>=2){
		FILE *batch;
		char * line = NULL;
		size_t lineLength = 0;
		size_t readLine;
		//the words are separated by a whitespace in the file
		char delimitter[] = " ";

		if (argc>2){
			write(STDERR_FILENO,file_number_error,strlen(file_number_error));
			exit(1);
		}
		else{
			batch = fopen(argv[1],"r");
			if (batch == NULL){
				write(STDERR_FILENO,file_error,strlen(file_error));
			}
			else{
			
			while ((readLine = getline(&line, &lineLength,batch)) != -1){
				char *fileArray[100]={'\0'};
				int n = 0; 

				
				//the following line removes end-of-lines characters thanks to \r
				line[strcspn(line, "\r\n")] = 0;
			
				char *word = strtok(line,delimitter);
		
				//loop for adding every separate word from line to the array
				while(word != NULL){
					fileArray[n] = word;
					word = strtok(NULL,delimitter);
					n++;
		
				}
				//if condition to exit, we first check if the first input word is 				exit	
				if (strcmp("exit",fileArray[0])==0){
					if(fileArray[1]==NULL){
						exit(0);
					}
					else{
				write(STDERR_FILENO,parameter_message,strlen(parameter_message));
					}
						
				
				}
				else if(strcmp("cd",fileArray[0])==0){
					changeDir(fileArray);
					
				}
				else if(strcmp("path",fileArray[0])==0){
					//we set the value of path to be the return value of the 					getPath function
					path = getPath(fileArray);
				}
		
				else{
					//we create a child process for the execution of a new command
					pid_t process=fork();
					if (process == 0){
						executeCommand(path, fileArray);
					}
					else{
						wait(&process);
					}	
				
				}

				
			}	
			}
		fclose(batch);
		}
		
		
	}

/*Implementation of interactive shell mode in else*/
	else{
		
		char * buffer;
		while(1){
			
			size_t bufsize = 100;
			size_t characters;
			//the standard input takes line with words separated by space
			char delim[]= " ";
			//array to stock different words of standard input with each element as empty at the beginning of the loop
			char *array[100]={'\0'};
			int i=0;

			//buffer to get the input	
			buffer = (char *)malloc(bufsize * sizeof(char));
			if (buffer == NULL){
			perror("Unable to allocate buffer");
			exit(1);
			}

			printf("Wish> ");
			characters = getline(&buffer, &bufsize,stdin);
		
			//the following line removes end-of-lines characters thanks to \r
			buffer[strcspn(buffer, "\r\n")] = 0;
			
			char *token = strtok(buffer,delim);
		
			//loop for adding every separate word from input to the array
			while(token != NULL){
				array[i] = token;
				token = strtok(NULL,delim);
				i++;
		
			}

			//if condition to exit, we first check if the first input word is exit	
			if (strcmp("exit",array[0])==0){
					if(array[1]==NULL){
						exit(0);
					}
					else{
				write(STDERR_FILENO,parameter_message,strlen(parameter_message));
					}
						
				
			}
			else if(strcmp("cd",array[0])==0){
				changeDir(array);
				
			}
			else if(strcmp("path",array[0])==0){
				//we set the value of path to be the return value of the getPath function
				path = getPath(array);
			}
		
			else{
			//we create a child process for the execution of a new command
			pid_t process=fork();
			if (process == 0){
				executeCommand(path, array);
			}
			else{
				wait(&process);
			}	
				
			}
		}
		}
	
}

//definition of the function executeCommand 

void executeCommand(char * path, char *array[]){
	if (execv(path,array)==-1){
		write(STDERR_FILENO,command_not_found,strlen(command_not_found));
	}
	else{
		execv(path,array);
	}

}

//definition of the function changeDir

void changeDir(char *array[]){
	int i = 1; 
	if(array[1]==NULL){
	write(STDERR_FILENO,parameter_message,strlen(parameter_message));
	}
	else if(array[2] != NULL){
	write(STDERR_FILENO,parameter_message,strlen(parameter_message));
	}
	else{
	char *dir = getenv(array[1]);
	int cdr = chdir(dir);
					
	if(cdr != 0){
		write(STDERR_FILENO,directory_message,strlen(directory_message));
	}	
	}
	
}

//definition of the function getPath
char* getPath(char *array[]){
		int i = 1;
		char * path;
		if (array[i] == NULL){
			path = array[i];
		}
		else{
			while (array[i] != NULL){
				path = array[i];
				i++;
			}
		}
		return path;
}
