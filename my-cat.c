#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
/*If the user has given more than 2 arguments we can take the second one and the following ones as file names*/
	if(argc >= 2){
	
		FILE *file;
		char line;
		int i;
		
		/*while argv[] has arguments, we save the parameter of index i as filename and print out if contents if the file is openable, if it is not, we exit with an error.*/
		for (i=1;i<argc;i++){
			file = fopen(argv[i],"r");
			if (file == NULL){
				printf("my-cat: cannot open file.\n");
				exit(1);	
			}
		
			while((line = fgetc(file)) != EOF){
				printf("%c",line);
				}
			
		
		}
		fclose(file);
		exit(0);
	
	}
	/*If the user forgets to give arguments, we notify the user that the program needs at least one argument and exit the program*/
	else{
		printf("You need to enter at least one argument for the program to work.\n");
		exit(0);
	}

}

