/*link used to understand the return value of fread() https://stackoverflow.com/questions/28398255/fread-return-value-in-c*/

#include <stdlib.h>
#include <stdio.h>


int main(int argc, char *argv[]){
	FILE *file;
	int i,j;
	int count;
	char currentChar;
	size_t charSize;
	
	/*Checking if the file name is given*/
	if(argc<2){
		printf("my-unzip: file1 [file2 ...]\n");
		exit(1);

	}
	
	
	else{
		for (i=1;i<argc;i++){
		file = fopen(argv[i],"r+");
		
		/*attempt to open file*/
			if(file == NULL){
				printf("my-unzip: cannot open file.\n");
				exit(1);
			}
			/*loop for every new byte that is read, the read value becomes the number of times the same character is in the file in a row*/
			while((charSize=fread(&count,sizeof(int),i,file)) == 1){
			currentChar = fgetc(file);
				for(j = 0;j<count;j++){
					printf("%c",currentChar);
				} 
			}
			
			
		}
		exit(0);
	}
	
	
}

