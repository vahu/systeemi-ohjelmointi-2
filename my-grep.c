#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*Link used to find a function to find a substring inside a string https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm*/

int main(int argc,char *argv[]){

FILE *file;
int i;
char line[100]; 

//if the user doesn't give paramaters, error occurs
	if (argc <2){
		printf("my-grep: searchterm [file ...]\n");
		exit(1);
	}
	
//if the user gives a word to search and files to search from, we search for the word from every file at a time and print out the lines containing the wanted word
	
	else if (argc > 2){
		for (i=2;i<argc;i++){
			file = fopen(argv[i],"r+");
		
			if(file == NULL){
				printf("my-grep: cannot open file.\n");
				exit(1);
			}
		
			while(fgets(line,99,file) != NULL){
				if (strstr(line,argv[1])){
					printf("%s",line);
				}
		
			}
	
		}
		fclose(file);
		exit(0);
	}
	
//if the user gives only the word to search but not a file name, we search from standard input
	else if (argc == 2){
		while(fgets(line,99,stdin) != NULL){
			if (strstr(line,argv[1])){
				printf("%s",line);
			}
		
		}
		exit(0);
	
	}

}
./
